# syntax = docker/dockerfile:1.2
FROM docker:latest

RUN apk add --no-cache curl bash python3

# use the package access token for the porter install url
# (see https://gitlab.com/gitlab-org/gitlab/-/issues/299384)
RUN --mount=type=secret,id=packagetoken (curl -L https://cdn.porter.sh/latest/install-linux.sh | bash) \
    && ln -s /root/.porter/porter /usr/local/bin/porter \
    && porter mixin install helm3-centros --version 0.3.0 --url https://$(cat /run/secrets/packagetoken)@gitlab.com/api/v4/projects/32106402/packages/generic/files/
